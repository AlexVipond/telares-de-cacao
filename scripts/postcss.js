const { exec } = require('@baleada/prepare')

module.exports = function() {
  const command = 'postcss ./src/style.css -o assets/style.css --verbose'
  exec(command)
}
