const tailwindcss = require('tailwindcss'),
      purgecss = require('@fullhuman/postcss-purgecss')

module.exports = {
  plugins: [
    require('postcss-import'),
    require('postcss-nested'),
    tailwindcss('./tailwind.config.js'),
    require('autoprefixer'),
    // purgecss({
    //   content: [
    //     'index.html',
    //   ],
    //   whitelist: [
    //     
    //   ],
    //   defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
    // }),
  ]
}
