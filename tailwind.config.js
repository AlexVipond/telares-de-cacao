const tailwind = require('tailwindcss/defaultTheme'),
      baleada = require('@baleada/tailwind-theme'),
      plugin = require('tailwindcss/plugin')

module.exports = {
  theme: {
    ...baleada,
    fontFamily: {
      sans: ['Open Sans', ...tailwind.fontFamily.sans],
      display: ['La Belle Aurore', ...tailwind.fontFamily.sans],
    },
    colors: {
      ...baleada.colors,
      primary: baleada.colors.green,
    },
    spacing: {
      ...baleada.spacing,
      '1/6': '16%',
      '1/5': '20%',
      '1/4': '25%',
      '1/3': '33%',
      '2/5': '40%',
      '2/3': '67%',
      '3/4': '75%',
    },
    screens: {
      xs: '380px',
      ...baleada.screens,
    }
  },
  // variants: baleada.variants,
  plugins: [
    require('@tailwindcss/custom-forms'),
  ]
}